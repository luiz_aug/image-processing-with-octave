% a02_08 [script]

clear all; close all; clc

g = checkerboard(50, [1 1]);


gnn = imrotate(g, 45, 'nearest');
gbc = imrotate(g, 45, 'bicubic');


figure,
imshow(g), title('g')
figure,
imshow(gnn), title('nn')
figure,
imshow(gbc), title('bc')