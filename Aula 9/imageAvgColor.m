function color = imageAvgColor(image)    avgRed = mean(mean(image(:,:,1)));    avgGreen = mean(mean(image(:,:,2)));    avgBlue = mean(mean(image(:,:,3)));        color = [avgRed avgGreen avgBlue];
endfunction

