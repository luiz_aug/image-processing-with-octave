%{ 
AULA 1 - Recorte de imagens
Descrição: Carregar uma imagem, descobrir as coordenadas de interesse para o corte,
           salvar o recorte feito e por fim sobreescrever a imagem original com 
           um retangulo preto indicando o local de recorte.
%}

clear all; 
close all; 
clc;

g = imread('42049_20-200.png');
figure, imshow(g)

% Coordenadas de interesse: Canto Superior Esquerdo & Canto Inferior Direito
#cse: 75,168
#cie: 266,281
crop = g(75:266,168:281);
figure, imshow(crop)

imwrite(crop, 'passaro.png');

% Coordenadas de interesse: Canto Superior Esquerdo & Canto Inferior Direito
#cse: 75,168
#cie: 266,281
g2 = g;
g2(75:266,168:281) = 0;
figure, imshow(g2)


