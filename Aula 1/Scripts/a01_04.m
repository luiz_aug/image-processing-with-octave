%{ 
AULA 1 - Transformação entre classes de imagens
Descrição: Transformar imagens entre diversas classes.
%}

clear all; close all; clc

g = imread('42049_20-200.png');

mg = min(g(:)); Mg = max(g(:));
figure, imshow(g), title('g');

g1 = double(g); mg1=min(g1(:)); Mg1=max(g1(:));
figure, imshow(g1); title('g1')

g2 = im2double(g); mg2=min(g2(:)); Mg2=max(g2(:));
figure, imshow(g2), title('g2')

g3 = mat2gray(g); mg3=min(g3(:)); Mg3=max(g3(:));
figure, imshow(g3), title('g3')

%{
  g é da classe: uint8
  Min em g: 20, Max em g: 200 
  
  g1 é da classe: double
  Min. em g1: 20, Max em g1: 200
  imshow(g1) mostra uma imagem: Não
  porque: A função double apenas transforma o array em formato double,
          mantendo o seu valor. Para que este array seja utilizado pela
          função imshow é necessário ainda converter os seus valores para
          um intervalo [0,1].
  
  g2 é da classe:  double
  Min em g2: 0.078431, Max em g2: 0.78431
  
  g3 é da classe: 
  Min em g3:  0, Måx em g3: 1
  Qual é a diferença entre as funções im2double e mat2gray?
  A função im2double não normaliza a imagem original ao criar o array de doubles.
  Por outro lado, a função img2gray o faz, gerando um valor minimo de 0 e maximo 1.
%}