%{ 
AULA 1 - Normalização e Auto-Contraste
Descrição: Utilizar a função mat2gray para normalizar a 
           imagem carregada e assim aplicar o auto-contraste.
%}

clear all; close all; clc

g = imread('42049_20-200.png');
gac = mat2gray(g);

figure
subplot(1,2,1)
imshow(g), title('Original')
subplot(1,2,2)
imshow(gac), title('Autocontrast')