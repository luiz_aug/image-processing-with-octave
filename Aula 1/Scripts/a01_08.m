% a01_08 [script]

clear all; close all; clc

g = imread('42049.jpg');
u = unique(g);
ug = size(u, 1);

figure, imshow(g)
unstr = num2str(ug);
title(['ug = ' unstr])