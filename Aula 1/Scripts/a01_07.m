%{ 
AULA 1 - Recorte de imagens
Descrição: Carregar uma imagem, descobrir as coordenadas de interesse para o corte,
           salvar o recorte feito e por fim sobreescrever a imagem original com 
           um retangulo preto indicando o local de recorte.
%}

clear all; close all; clc

v = 0:1:255;

s = repmat(v, 200, 1);
sb = s;
sb(1,1:end) = 0; %sup
sb(1:end,end) = 0; %dir
sb(end,1:end) = 0; %inf
sb(1:end,1) = 0; %esq
sb = uint8(sb);

figure, imshow(sb), title('sb')

sb2 = uint8(zeros(52,258));
sb2(2:51,2:257) = s;
figure, imshow(sb2), title('sb2')

imwrite(sb, 'grays8bit.png');