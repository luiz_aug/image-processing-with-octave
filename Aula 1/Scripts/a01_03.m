%{ 
AULA 1 - Ajuste de Contraste
Descrição: Selecionar pixels de uma imagem acima e abaixo de um certo limiar e 
selecionar novo valor para estes pixels.
%}

% Preparar Ambiente de Trabalho
clear all; 
close all; 
clc;

% Ler imagem e guardar original
c =  imread('../Files/42049.jpg');  % 8 bits por pixel - grayscale [0...255]
orig = c;

% Processar máximos e minimos originais
m = min(orig(:));
M = max(orig(:));

% Logical indexing
cL = c < 20;
c(cL) = 20;
cL = c > 200;
c(cL) = 200;

% Mostrar Original
title2 = sprintf("MIN: %d  MAX: %d",m,M);
figure, imshow(orig), title(["Original";title2]);

% Mostrar Editado
m = min(c(:));
M = max(c(:));
title2 = sprintf("MIN: %d  MAX: %d",m,M);
figure, imshow(c), title(["Edited";title2]);

% Salvar Imagem Editada
imwrite(c, '42049_20-200.png');

