# AULA 5 - Filtro Adaptavido
# Descrição: Explorar o funcionamento de um filtro adaptativo do tipo MMSE
# Out. 2020

# Environment Setup
pkg load image;
clear all; 
close all; 
clc

# Load Image
originalImage = imread('../Files/Lenna256g.png');
figure, imshow(originalImage), title('Original')

% Noise addition (gaussian, stdev = 10)
noisyImage = imnoise(originalImage,'gaussian',(0/255),(10/255)^2);
figure, imshow(originalImage), title('Com ruido')

# User interface for noisy area selection
disp('Selecione uma região para estimar a variância do ruído.')
[reg, rect] = imcrop(noisyImage);
% rect = [xmin ymin width height]
imin = ceil(rect(2)); %linha cse
jmin = ceil(rect(1)); %coluna cse
imax = floor(rect(2)+rect(4)); %linha cid
jmax = floor(rect(1)+rect(3)); %coluna cid
disp('Coodenadas da região selecionada')
disp(['Linha do canto superior esquerdo: ' num2str(imin)])
disp(['Coluna do canto superior esquerdo: ' num2str(jmin)])
disp(['Linha do canto inferior direito: ' num2str(imax)])
disp(['Coluna do canto inferior direito: ' num2str(jmax)])

# Noise statistical processing
noisyImage = double(noisyImage);
noise_stdev = var(double(reg(:)));

nrcw = 5; % numero de linhas e colunas da janela

# Finding window standard deviations
nlFunction = @(x)var(x(:));
local_stdev = nlfilter(noisyImage,[nrcw nrcw],nlFunction);
figure, imshow(mat2gray(local_stdev)), title('Variâncias')

% Finding window averages
h = fspecial('average', [nrcw nrcw]);
local_avg = imfilter(noisyImage, h, 0);
figure, imshow(uint8(local_avg)), title('Média 5x5');

# Filtering image (combine window stdev, window average and noise stdev)
outImage = (1 - (noise_stdev./local_stdev)).*noisyImage + (noise_stdev./local_stdev).*local_avg;
outImage = uint8(outImage);
figure, imshow(outImage), title('MMSE 5x5');

# Comments:
# Esse filtro funciona discriminando regioões homogeneas (stdev aprox. 0) e regiões de detalhes (stdev grande)
# na hora de ponderar a aplicação do filtro passa baixa, de forma a diminuir a perda de informações nas 
# regiões de detalhes.
#
# Em uma região homogênea, a variância do ruído é aproximadamente igual à variância local, então Vr/Vl tende a 1 e
# é dado maior peso para o pixel do filtro passa baixas.
#
# Em uma região de bordas, a variãncia local é muito maior que a variância do ruído, então Vr/Vl tende a 0 e
# é dado maior peso para o pixel original da imagem.