% a05_04 [script]

clear all; close all; clc

% SNRL = Ms/SDn
% Ms � a m�dia do sinal, estimada considerando-se os pixels de uma regi�o
% clara (sinal) e homog�nea (sem bordas)  da  imagem. SDn � o desvio padr�o
% do ru�do, estimado considerando-se os pixels de uma regi�o escura
% (sem sinal, portanto, apenas ru�do) e homog�nea da imagem.

g1 = imread('Lenna256g_ng20.png');

% Intera��o para selecionar regi�es na g1
disp('Selecione uma regi�o para estimar a m�dia do sinal.')
figure
[reg_s, rect_s] = imcrop(g1); % rect_s ser� par�metro pra imcrop 
disp('Selecione uma regi�o para estimar a vari�ncia do ru�do.')
[reg_n, rect_n] = imcrop(g1); % rect_n ser� par�metro pra imcrop 
SNRLg1 = mean(double(reg_s(:)))/std(double(reg_n(:)))

h = fspecial('average', [3 3]);
g2 = imfilter(g1, h);
h = fspecial('average', [7 7]);
g3 = imfilter(g1, h);

% Para g2 usa mesmas regi�es
[reg_s] = imcrop(g2, rect_s);
[reg_n] = imcrop(g2, rect_n);
SNRLg2 = mean(double(reg_s(:)))/std(double(reg_n(:)))

% Para g3 usa mesmas regi�es
[reg_s] = imcrop(g3, rect_s);
[reg_n] = imcrop(g3, rect_n);
SNRLg3 = mean(double(reg_s(:)))/std(double(reg_n(:)))

figure, imshow(g1), title('g1')
figure, imshow(g2), title('g2')
figure, imshow(g3), title('g3')