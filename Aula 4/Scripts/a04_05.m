# AULA 4 - Filtros - Filtro gaussiano vs Média
# Descrição: Usar as funções imfilter e fspecial com o 
#            filtro Gaussiano de janela 5-por-5 e sigma 1 para 
#            suavizar o ruído das imagens de RM b5s.40.bmp e b5s.100.bmp
# Luiz A. Bernardi - 943983
# Set. 2020

# Environment Setup
pkg load image
clear all; clc; close all;

# Load images:
g1 = imread('../Files/b5s.40.bmp');
g2 = imread('../Files/b5s.100.bmp');

# Create filter masks:
hm5 = fspecial('average', [5 5]); 
hg5 = fspecial('gaussian', [5 5], 1);

# Filter image:
g1hm5 = imfilter(g1, hm5);
g1hg5 = imfilter(g1, hg5);

g2hm5 = imfilter(g2, hm5);
g2hg5 = imfilter(g2, hg5);

# Show results: 
figure
subplot(1,3,1), imshow(g1), title('original')
subplot(1,3,2), imshow(g1hm5), title('média 5x5')
subplot(1,3,3), imshow(g1hg5), title('gaussiano 5x5 sigma=1')

figure
subplot(1,3,1), imshow(g2), title('original')
subplot(1,3,2), imshow(g2hm5), title('média 5x5')
subplot(1,3,3), imshow(g2hg5), title('gaussiano 5x5 sigma=1')