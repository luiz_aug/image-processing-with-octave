# AULA 4 - Filtros - Filtro gaussiano
# Descrição: Avaliar a expressão: 
#            O principal parâmetro da função Gaussiana 
#            é o desvio padrão, sigma, que controla a “abertura” da curva. 
#            É importante observar que, se o desvio padrão for muito alto e as 
#            dimensões da máscaras pequenas, a função Gaussiana pode “não caber 
#            na máscara.
#            Usar as funções imfilter e fspecial com o filtro da média 5x5 e 7x7 
# Luiz A. Bernardi - 943983
# Set. 2020

# Environment Setup
pkg load image
clear all; clc; close all;

# Parameters: 'gaussian', [dimension], variance
h1 = fspecial('gaussian', [11 11], 0.5);
sum(h1(:)) % verify normalization (sum of coefficients = 1)
h2 = fspecial('gaussian', [11 11], 2);
sum(h2(:)) % verify normalization (sum of coefficients = 1)
h3 = fspecial('gaussian', [11 11], 5);
sum(h3(:)) % verify normalization (sum of coefficients = 1)
h4 = fspecial('gaussian', [11 11], 30);
sum(h4(:)) % verify normalization (sum of coefficients = 1)

# Showing results
figure
x = -5:5;
i = 5;
plot(x,h1(i,:),x,h2(i,:),...
    x,h3(i,:),x,h4(i,:)),
legend('0.5','2',...
    '5', '30')
title(['Perfil da linha ',...
    num2str(i)],'FontWeight','bold')