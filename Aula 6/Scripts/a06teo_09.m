% a06teo_09 [script]
clear all, close all, clc
 
% Gera um LoG de sigma=4 em uma janela 31x31
% � 31x31 pois o objetivo � apenas visualizar
% uma LoG e n�o convoluir com uma imagem
logf = fspecial('log', [31 31], 4);
p = logf(16,:); %linha central
 
%Dysplay
figure
mesh(-logf, 'EdgeColor', 'black')
title('LoG sigma=4')
figure
plot(1:31,-p,'LineWidth',2)
xlim([1 31])
grid
title('Perfil do LoG')